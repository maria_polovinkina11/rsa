﻿namespace RSA
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_decrypt = new System.Windows.Forms.TextBox();
            this.textBox_encrypt = new System.Windows.Forms.TextBox();
            this.textBox_O_key = new System.Windows.Forms.TextBox();
            this.textBox_z_key = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton_encr = new System.Windows.Forms.RadioButton();
            this.radioButton_decr = new System.Windows.Forms.RadioButton();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.load_file = new System.Windows.Forms.Button();
            this.start = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.unload_file = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_decrypt
            // 
            this.textBox_decrypt.Location = new System.Drawing.Point(40, 40);
            this.textBox_decrypt.Multiline = true;
            this.textBox_decrypt.Name = "textBox_decrypt";
            this.textBox_decrypt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_decrypt.Size = new System.Drawing.Size(436, 366);
            this.textBox_decrypt.TabIndex = 0;
            // 
            // textBox_encrypt
            // 
            this.textBox_encrypt.Location = new System.Drawing.Point(645, 40);
            this.textBox_encrypt.Multiline = true;
            this.textBox_encrypt.Name = "textBox_encrypt";
            this.textBox_encrypt.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_encrypt.Size = new System.Drawing.Size(436, 366);
            this.textBox_encrypt.TabIndex = 1;
            // 
            // textBox_O_key
            // 
            this.textBox_O_key.Location = new System.Drawing.Point(40, 476);
            this.textBox_O_key.Name = "textBox_O_key";
            this.textBox_O_key.Size = new System.Drawing.Size(209, 22);
            this.textBox_O_key.TabIndex = 2;
            // 
            // textBox_z_key
            // 
            this.textBox_z_key.Location = new System.Drawing.Point(40, 545);
            this.textBox_z_key.Name = "textBox_z_key";
            this.textBox_z_key.Size = new System.Drawing.Size(209, 22);
            this.textBox_z_key.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(37, 456);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Открытый ключ ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton_encr);
            this.groupBox1.Controls.Add(this.radioButton_decr);
            this.groupBox1.Location = new System.Drawing.Point(445, 444);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(313, 39);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // radioButton_encr
            // 
            this.radioButton_encr.AutoSize = true;
            this.radioButton_encr.Checked = true;
            this.radioButton_encr.Location = new System.Drawing.Point(174, 12);
            this.radioButton_encr.Name = "radioButton_encr";
            this.radioButton_encr.Size = new System.Drawing.Size(121, 21);
            this.radioButton_encr.TabIndex = 1;
            this.radioButton_encr.TabStop = true;
            this.radioButton_encr.Text = "Зашифровать";
            this.radioButton_encr.UseVisualStyleBackColor = true;
            // 
            // radioButton_decr
            // 
            this.radioButton_decr.AutoSize = true;
            this.radioButton_decr.Location = new System.Drawing.Point(0, 12);
            this.radioButton_decr.Name = "radioButton_decr";
            this.radioButton_decr.Size = new System.Drawing.Size(128, 21);
            this.radioButton_decr.TabIndex = 0;
            this.radioButton_decr.Text = "Расшифровать";
            this.radioButton_decr.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // load_file
            // 
            this.load_file.Location = new System.Drawing.Point(450, 511);
            this.load_file.Name = "load_file";
            this.load_file.Size = new System.Drawing.Size(123, 51);
            this.load_file.TabIndex = 9;
            this.load_file.Text = "Загрузить файл";
            this.load_file.UseVisualStyleBackColor = true;
            this.load_file.Click += new System.EventHandler(this.load_file_Click);
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(958, 476);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(123, 51);
            this.start.TabIndex = 10;
            this.start.Text = "Выполнить";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(37, 525);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(116, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "Закрытый ключ ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(87, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(239, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "Исходный/ расшифрованный текст";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(783, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 17);
            this.label4.TabIndex = 13;
            this.label4.Text = "Зашифрованный текст";
            // 
            // unload_file
            // 
            this.unload_file.Location = new System.Drawing.Point(640, 508);
            this.unload_file.Name = "unload_file";
            this.unload_file.Size = new System.Drawing.Size(123, 51);
            this.unload_file.TabIndex = 8;
            this.unload_file.Text = "Выгрузить файл";
            this.unload_file.UseVisualStyleBackColor = true;
            this.unload_file.Click += new System.EventHandler(this.unload_file_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1171, 640);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.start);
            this.Controls.Add(this.load_file);
            this.Controls.Add(this.unload_file);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_z_key);
            this.Controls.Add(this.textBox_O_key);
            this.Controls.Add(this.textBox_encrypt);
            this.Controls.Add(this.textBox_decrypt);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_decrypt;
        private System.Windows.Forms.TextBox textBox_encrypt;
        private System.Windows.Forms.TextBox textBox_O_key;
        private System.Windows.Forms.TextBox textBox_z_key;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton_encr;
        private System.Windows.Forms.RadioButton radioButton_decr;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button load_file;
        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button unload_file;
    }
}

