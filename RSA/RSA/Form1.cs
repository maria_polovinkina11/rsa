﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace RSA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
			openFileDialog1.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";//открываем файл
			saveFileDialog1.Filter = "Text files(*.txt)|*.txt|All files(*.*)|*.*";//записываем текст в файл
		}
        string in_txt, dec_txt;
		string S;
		int E, n, d;
		
		byte[] mas_inp;
		int[] mas;
		private void load_file_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = openFileDialog1.FileName;
            // читаем файл в строку
            string fileText = System.IO.File.ReadAllText(filename);
            textBox_decrypt.Text = fileText;
            in_txt = fileText;
            MessageBox.Show("Файл открыт");
        }

        private void unload_file_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            // получаем выбранный файл
            string filename = saveFileDialog1.FileName;

            // сохраняем текст в файл
            System.IO.File.WriteAllText(filename, textBox_encrypt.Text);

            MessageBox.Show("Файл сохранен");
        }

        private void start_Click(object sender, EventArgs e)//кнопка "Выполнить"
        {														   
			if (radioButton_encr.Checked == true)//если выбран - Зашифровать
		 {
				textBox_encrypt.Text = "";
				S = textBox_decrypt.Text;
			
	

					mas_inp  = Encoding.ASCII.GetBytes(S);

				mas = new int[mas_inp.Length];

				Encode(out E, out n, out d);

					textBox_O_key.Text = Convert.ToString(E);//выводим в текстбокс для открытого ключа
					textBox_z_key.Text = Convert.ToString(d);//выводим в теккстбок для закрытого ключа
					for (int i = 0; i < mas_inp.Length; i++)
					{
						mas[i] = (int)BigInteger.ModPow(mas_inp[i], E, n);
						textBox_encrypt.Text += Convert.ToString(mas[i]);
					    textBox_encrypt.Text += " ";
		         	}

			}
			else //расшифровать
			{
				for (int i = 0; i < mas_inp.Length; i++)
				{
					mas_inp[i] = (byte)BigInteger.ModPow(mas[i], d, n);
				}

				dec_txt = Encoding.ASCII.GetString(mas_inp);
				
			}
		}

	
		static private bool Testferma(long x)//тест Ферма
		{
			
			Random rand = new Random();
			if (x == 0)
				return false;
			if (x == 2)
				return true;

			for (int i = 0; i < 100; i++)
			{
				long a = rand.Next((int)x);
				if (a <= 2)
					a = a + 2;
				if (BigInteger.ModPow(a, x - 1, x) != 1)
				{
					return false;
				}
			}
			return true;
		}

	

		long GCD(long a, long b)
		{
			while (a != 0 && b != 0)
			{
				if (a >= b) a = a % b;
				else b = b % a;
			}
			return a + b; // Одно - ноль
		}
		 int Gcd(int a, int b, out int x, out int y)
		{
			if (a == 0)
			{
				x = 0; y = 1;
				return b;
			}
			int x1, y1;
			int d = Gcd(b % a, a, out x1, out y1);
			x = y1 - (b / a) * x1;
			y = x1;
			return d;
		}
		private  void Encode(out int e, out int n, out int d)
		{

			int p, q, w, fi;
			bool c;
			Random rand = new Random();
			do
			{
				p = rand.Next(32000);
				c = Testferma(p);
			}
			while (!c);
			q = p;
			do
			{
				q++;
				c = Testferma(q);
			}
			while (!c);
			n = p * q;
			fi = (p - 1) * (q - 1);
			do
			{
				e = rand.Next(6000);
				Gcd(e, fi, out w, out d);
			}
			while ((GCD(fi, e) != 1) || (w < 0));
			d = w;
		}


	}
}
